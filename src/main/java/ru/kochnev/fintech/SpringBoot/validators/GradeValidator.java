package ru.kochnev.fintech.SpringBoot.validators;

import lombok.RequiredArgsConstructor;
import ru.kochnev.fintech.SpringBoot.course.Course;
import ru.kochnev.fintech.SpringBoot.course.CourseService;
import ru.kochnev.fintech.SpringBoot.student.StudentService;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;
import java.util.List;
import java.util.UUID;

@SupportedValidationTarget(ValidationTarget.PARAMETERS)
@RequiredArgsConstructor

public class GradeValidator implements
        ConstraintValidator<GradeValid, Object[]> {


    private final CourseService courseService;
    private final StudentService studentService;

    @Override
    public void initialize(GradeValid gradeValid) {
    }

    @Override
    public boolean isValid(Object[] value, ConstraintValidatorContext cxt) {

        Course course = courseService.findById(UUID.fromString((String) value[1]));

        for (String studentId : (List<String>) value[0]) {
            if (studentService
                    .findById(UUID.fromString(studentId))
                    .getGrade() < course.getRequiredGrade()) {
                return false;
            }
        }
        return true;
    }
}