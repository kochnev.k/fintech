package ru.kochnev.fintech.SpringBoot.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = GradeValidator.class)
@Target( { ElementType.METHOD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface GradeValid {
    String message() default "Higher grade required";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
