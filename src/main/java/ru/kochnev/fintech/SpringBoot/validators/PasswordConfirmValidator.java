package ru.kochnev.fintech.SpringBoot.validators;

import ru.kochnev.fintech.SpringBoot.user.User;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordConfirmValidator implements ConstraintValidator<PasswordConfirm, Object> {

    @Override
    public void initialize(PasswordConfirm constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context){
        User user = (User) object;
        return user.getPassword().equals(user.getPasswordConfirm());
    }
}
