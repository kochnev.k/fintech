package ru.kochnev.fintech.SpringBoot.userrole;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
public class UserRole {
    private UUID userId;
    private UUID roleId;
}
