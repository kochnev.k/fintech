package ru.kochnev.fintech.SpringBoot.course;

import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.kochnev.fintech.SpringBoot.exceptions.MyException;
import ru.kochnev.fintech.SpringBoot.validators.GradeValid;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequestMapping("/course")
@RequiredArgsConstructor
public class CourseController {

    private final CourseService courseService;

    @PostMapping
    public void save(@RequestBody @Valid CourseRequest courseRequest) {
        courseService.save(courseRequest);
    }

    @GetMapping("/{id}")
    public CourseRequest getById(@PathVariable("id") String id) throws MyException {
        return courseService.getById(id);
    }

    @PutMapping
    public void update(@RequestBody @Valid CourseRequest courseRequest) {
        courseService.update(courseRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") String id) {
        courseService.deleteById(id);
    }

    @GradeValid
    @PatchMapping("/{id}")
    public void addStudentToCourse(@RequestBody List<String> studentsIdList, @PathVariable("id") String courseId) throws MyException {
        courseService.addStudentToCourse(studentsIdList, courseId);
    }
}