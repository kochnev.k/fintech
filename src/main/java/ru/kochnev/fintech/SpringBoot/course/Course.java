package ru.kochnev.fintech.SpringBoot.course;

import lombok.*;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@Accessors(chain = true)
public class Course {
    private UUID id = UUID.randomUUID();
    @NonNull
    private String name;
    @NonNull
    private String description;
    @NonNull
    private int requiredGrade;
}