package ru.kochnev.fintech.SpringBoot.course;

import lombok.*;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;


@Data
@NoArgsConstructor
@Accessors(chain = true)
public class CourseRequest {
    private UUID id = UUID.randomUUID();
    @NotBlank(message = "name of course cannot be empty ")
    private String name;
    @NotBlank(message = "description cannot be empty ")
    private String description;
    @NotNull(message = "required_grade cannot be empty ")
    private int requiredGrade;
}
