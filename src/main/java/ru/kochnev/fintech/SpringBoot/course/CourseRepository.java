package ru.kochnev.fintech.SpringBoot.course;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.UUID;

@Mapper
public interface CourseRepository {

    @Insert("INSERT INTO courses (id, name, description, required_grade) " +
            "VALUES (#{id}::uuid, #{name}, #{description}, #{requiredGrade})")
    public int insert(Course course);

    @Select("SELECT id, name, description, required_grade FROM courses")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "requiredGrade", column = "required_grade")
    })
    public List<Course> findAll();

    @Select("SELECT id, name, description, required_grade FROM courses WHERE id = #{id}::uuid")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "requiredGrade", column = "required_grade")
    })
    public Course findById(UUID id);

    @Select("SELECT TOP 1 *, (" +
            "SELECT AVG(age) FROM students " +
            "WHERE students.course_id = courses.id" +
            ") AS avg_age FROM courses " +
            "ORDER BY avg_age DESC")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(property = "description", column = "description"),
            @Result(property = "requiredGrade", column = "required_grade")
    })
    public Course findByMaxAvgAge();

    @Update("UPDATE courses SET name = #{name}, " +
            "description = #{description}, required_grade = #{requiredGrade} WHERE id = #{id}::uuid")
    public int update(Course course);

    @Delete("DELETE FROM courses WHERE id = #{id}::uuid")
    public int deleteById(UUID id);
}
