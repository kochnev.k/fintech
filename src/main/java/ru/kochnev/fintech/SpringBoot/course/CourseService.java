package ru.kochnev.fintech.SpringBoot.course;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import ru.kochnev.fintech.SpringBoot.courseLRUCache.CourseLRUCache;
import ru.kochnev.fintech.SpringBoot.exceptions.MyException;
import ru.kochnev.fintech.SpringBoot.student.StudentRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;
    private final StudentRepository studentRepository;
    private final ModelMapper modelMapper;
    private final CourseLRUCache<UUID, Course> courseLRUCache;

    public Course findById(UUID id) {
        Optional<Course> optionalCourse = Optional.ofNullable(courseRepository.findById(id));
        return optionalCourse.orElseThrow(() -> new MyException("Course not found"));
    }

    public void save(CourseRequest courseRequest) {
        Course newCourse = modelMapper.map(courseRequest, Course.class);
        courseRepository.insert(newCourse);
        courseLRUCache.put(newCourse.getId(), newCourse);
    }

    public CourseRequest getById(String id) throws MyException {
        Course courseFromCache = courseLRUCache.get(UUID.fromString(id));
        if (courseFromCache == null) {
            Course courseFromDB = findById(UUID.fromString(id));
            courseLRUCache.put(courseFromDB.getId(), courseFromDB);
            return modelMapper.map(courseFromDB, CourseRequest.class);
        }
        return modelMapper.map(courseFromCache, CourseRequest.class);
    }

    public void update(CourseRequest updatedCourseRequest) {
        Course updatedCourse = modelMapper.map(updatedCourseRequest, Course.class);
        if (courseRepository.update(updatedCourse) > 0) {
            courseLRUCache.replace(updatedCourse.getId(), updatedCourse);
        } else {
            throw new MyException("Error updating course. Course not found");
        }
    }

    public void deleteById(String id) {
        if (courseRepository.deleteById(UUID.fromString(id)) > 0) {
            courseLRUCache.remove(UUID.fromString(id));
        } else {
            throw new MyException("Error deleting course. Course not found");
        }
    }

    public void addStudentToCourse(List<String> studentIdList, String courseId) {
        if (studentRepository.addStudentsToCourse(studentIdList, UUID.fromString(courseId)) == 0) {
            throw new MyException("Error adding student to course");
        }
    }
}


