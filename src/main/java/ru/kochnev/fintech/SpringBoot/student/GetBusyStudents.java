package ru.kochnev.fintech.SpringBoot.student;

import com.opencsv.bean.CsvToBeanBuilder;
import lombok.Getter;
import lombok.extern.java.Log;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@EnableScheduling
@Log
public class GetBusyStudents {

    @Getter
    private static final List<Student> studentList = new ArrayList<>();


    public GetBusyStudents(@Value("${default-pathname}") String pathname) throws FileNotFoundException {

        File file = new File(pathname);

        studentList.addAll(new CsvToBeanBuilder<Student>(new FileReader(file))
                .withType(Student.class)
                .build()
                .parse()
        );
    }

    @Scheduled(fixedRate = 3600000)
    @CountStudents
    public List<Student> getBusyStudents() {

        int currentHour = new DateTime().getHourOfDay();

        List<Student> busyStudentsList = studentList
                .stream()
                .filter(s -> s.getTimeFrom() <= currentHour && s.getTimeTo() >= currentHour)
                .collect(Collectors.toList());


        log.info(() -> String.format("List of busy students: %s", busyStudentsList));

        return busyStudentsList;
    }
}
