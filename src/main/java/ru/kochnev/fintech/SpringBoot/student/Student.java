package ru.kochnev.fintech.SpringBoot.student;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.kochnev.fintech.SpringBoot.course.Course;
import ru.kochnev.fintech.SpringBoot.validators.GradeValid;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class Student {
    private UUID id = UUID.randomUUID();
    @CsvBindByName(column = "name")
    private String name;
    @CsvBindByName(column = "age")
    private int age;
    @CsvBindByName(column = "time_from")
    private int timeFrom;
    @CsvBindByName(column = "time_to")
    private int timeTo;
    private int grade;

    private Course course;
}
