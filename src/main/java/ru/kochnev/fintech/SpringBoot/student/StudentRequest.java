package ru.kochnev.fintech.SpringBoot.student;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.kochnev.fintech.SpringBoot.course.CourseRequest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class StudentRequest {
    private UUID id = UUID.randomUUID();
    @NotBlank(message = "name of student cannot be empty ")
    private String name;
    @NotNull(message = "age cannot be empty ")
    private int age;
    @NotNull(message = "time_from cannot be empty ")
    private int timeFrom;
    @NotNull(message = "time_to cannot be empty ")
    private int timeTo;
    @NotNull(message = "grade cannot be empty ")
    private int grade;

    @JsonProperty("course")
    @NotNull(message = "course cannot be empty ")
    private CourseRequest courseRequest;
}
