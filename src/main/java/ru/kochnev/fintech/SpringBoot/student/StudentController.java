package ru.kochnev.fintech.SpringBoot.student;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.kochnev.fintech.SpringBoot.exceptions.MyException;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public void save(@RequestBody @Valid StudentRequest studentRequest) throws JsonProcessingException {
        studentService.save(studentRequest);
    }

    @GetMapping("/{id}")
    public StudentRequest getById(@PathVariable("id") String id) throws MyException {
        return studentService.getById(UUID.fromString(id));
    }

    @PutMapping
    public void update(@RequestBody @Valid StudentRequest studentRequest) throws JsonProcessingException {
        studentService.update(studentRequest);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") String id) throws JsonProcessingException {
        studentService.deleteById(id);
    }
}