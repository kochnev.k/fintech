package ru.kochnev.fintech.SpringBoot.student;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.UUID;

@Mapper
public interface StudentRepository {

    @Insert("INSERT INTO students (id, name, age, grade, time_from, time_to, course_id) " +
            "VALUES (#{id}::uuid, #{name}, #{age}, #{grade}, #{timeFrom}, #{timeTo}, #{course.id}::uuid)")
    public int insert(Student students);

    @Select("SELECT students.*, courses.name AS course_name, courses.description AS course_description, courses.required_grade AS course_required_grade " +
            "FROM students " +
            "LEFT JOIN courses " +
            "ON students.course_id = courses.id")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(property = "age", column = "age"),
            @Result(property = "grade", column = "grade"),
            @Result(property = "timeFrom", column = "time_from"),
            @Result(property = "timeTo", column = "time_to"),
            @Result(property = "course.id", column = "course_id"),
            @Result(property = "course.name", column = "course_name"),
            @Result(property = "course.description", column = "course_description"),
            @Result(property = "course.requiredGrade", column = "course_required_grade")
    })
    public List<Student> findAll();

    @Select("SELECT students.*, courses.name AS course_name, courses.description AS course_description, courses.required_grade AS course_required_grade " +
            "FROM students " +
            "LEFT JOIN courses " +
            "ON students.course_id = courses.id " +
            "WHERE students.id = #{id}::uuid")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(column = "name", property = "name"),
            @Result(property = "age", column = "age"),
            @Result(property = "grade", column = "grade"),
            @Result(property = "timeFrom", column = "time_from"),
            @Result(property = "timeTo", column = "time_to"),
            @Result(property = "course.id", column = "course_id"),
            @Result(property = "course.name", column = "course_name"),
            @Result(property = "course.description", column = "course_description"),
            @Result(property = "course.requiredGrade", column = "course_required_grade")
    })
    public Student findById(UUID id);

    @Update("UPDATE students SET name = #{name}, " +
            "age = #{age}, grade = #{grade}, time_from = #{timeFrom}, time_to = #{timeTo} WHERE id = #{id}::uuid")
    public int update(Student student);

    @Update("<script> " +
                "UPDATE students " +
                "SET course_id = '${courseId}' " +
                    "<where> id IN " +
                        "<foreach item = 'item' index = 'index' collection = 'list' " +
                            "open = '(' separator = ',' close = ')'> " +
                            "#{item}::uuid " +
                        "</foreach> " +
                    "</where> " +
            "</script>")
    public int addStudentsToCourse(@Param("list") List<String> studentIdList, UUID courseId);

    @Delete("DELETE FROM students WHERE id = #{id}::uuid")
    public int deleteById(UUID id);
}