package ru.kochnev.fintech.SpringBoot.student;

import lombok.extern.java.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Aspect
@Component
@Log
public class CountOfObjectAspect<T> {

    private final Map<T, Integer> countOfObject = new HashMap<>();

    @Around("@annotation(CountStudents)")
    public void countOfObject(ProceedingJoinPoint joinPoint) throws Throwable {

        List<T> proceed = (List<T>) joinPoint.proceed();

        proceed.forEach(i -> countOfObject.put(i, countOfObject.getOrDefault(i, 0) + 1));

        log.info(() -> String.format("Count of object: %s", countOfObject.toString()));

    }
}
