package ru.kochnev.fintech.SpringBoot.student;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kochnev.fintech.SpringBoot.exceptions.MyException;
import ru.kochnev.fintech.SpringBoot.outbox.OutboxEntity;
import ru.kochnev.fintech.SpringBoot.outbox.OutboxService;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final OutboxService outboxService;
    private final ModelMapper modelMapper;
    private final ObjectMapper mapper;

    public Student findById(UUID id) {
        Optional<Student> optionalStudent = Optional.ofNullable(studentRepository.findById(id));
        return optionalStudent.orElseThrow(() -> new MyException("Student not found"));
    }

    @Transactional
    public void save(StudentRequest studentRequest) throws JsonProcessingException {
        studentRepository.insert(modelMapper.map(studentRequest, Student.class));
        outboxService.save(mapper.writeValueAsString(studentRequest), OutboxEntity.Status.SAVE);
    }

    public StudentRequest getById(UUID id) throws MyException {
        return modelMapper.map(findById(id), StudentRequest.class);
    }

    @Transactional
    public void update(StudentRequest updatedStudentRequest) throws JsonProcessingException {
        if (studentRepository.update(modelMapper.map(updatedStudentRequest, Student.class)) == 0) {
            throw new MyException("Error updating student. Student not found");
        }
        outboxService.save(mapper.writeValueAsString(updatedStudentRequest), OutboxEntity.Status.UPDATE);
    }

    @Transactional
    public void deleteById(String id) throws JsonProcessingException {
        if (studentRepository.deleteById(UUID.fromString(id)) == 0) {
            throw new MyException("Error deleting student. Student not found");
        }
        outboxService.save(id, OutboxEntity.Status.DELETE);
    }
}
