package ru.kochnev.fintech.SpringBoot.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ru.kochnev.fintech.SpringBoot.outbox.OutboxEntity;
import ru.kochnev.fintech.SpringBoot.outbox.OutboxService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Component
@EnableScheduling
@RequiredArgsConstructor
public class Producer {

    private final ProducerService producerService;
    private final OutboxService outboxService;
    private final ObjectMapper mapper;

    @Scheduled(fixedRate = 60000)
    @Transactional
    public void publishAllPeriodically() {

        List<OutboxEntity> listMessagesFromOutbox = outboxService.findAll();

        List<UUID> uuidListSuccessfulMessages = new ArrayList<>();

        listMessagesFromOutbox.forEach(i -> {
            try {
                if (producerService.sendMessage(mapper.writeValueAsString(i))) {
                    uuidListSuccessfulMessages.add(i.getId());
                }
            } catch (ExecutionException | InterruptedException | JsonProcessingException e) {
                e.printStackTrace();
            }
        });

        outboxService.deleteById(uuidListSuccessfulMessages);
    }
}
