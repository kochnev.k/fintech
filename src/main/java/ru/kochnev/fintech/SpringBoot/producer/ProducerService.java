package ru.kochnev.fintech.SpringBoot.producer;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
public class ProducerService {

    private final KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka-topic}")
    private String topic;

    public boolean sendMessage(String message) throws ExecutionException, InterruptedException {

        ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topic, message);

        return future.get().getRecordMetadata().offset() >= 0;
    }
}
