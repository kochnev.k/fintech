package ru.kochnev.fintech.SpringBoot.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kochnev.fintech.SpringBoot.role.Role;
import ru.kochnev.fintech.SpringBoot.role.RoleRepository;
import ru.kochnev.fintech.SpringBoot.userrole.UserRole;
import ru.kochnev.fintech.SpringBoot.userrole.UserRoleRepository;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userMapper;

    private final RoleRepository roleMapper;

    private final UserRoleRepository userRoleMapper;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public User findUserById(UUID id) {
        return userMapper.findUserById(id);
    }

    @Transactional
    public void saveUser(User user) {
        user.setId(UUID.randomUUID());
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(1);
        userMapper.setUserInfo(user);
        user = userMapper.findUserById(user.getId());

        Role role = roleMapper.getRoleInfo("ROLE_USER");

        UserRole userRole = new UserRole();
        userRole.setRoleId(role.getId());
        userRole.setUserId(user.getId());

        userRoleMapper.setUserRoleInfo(userRole);
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userMapper.findUserByUsername(userName);
        return new UserPrincipal(user);
    }
}
