package ru.kochnev.fintech.SpringBoot.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kochnev.fintech.SpringBoot.role.Role;
import ru.kochnev.fintech.SpringBoot.validators.PasswordConfirm;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@PasswordConfirm
public class User {
    private UUID id;
    @NotBlank(message = "field \"password\" cannot be empty ")
    private String password;
    @NotBlank(message = "field \"password confirm\" cannot be empty ")
    private String passwordConfirm;
    @NotBlank(message = "field \"username\" cannot be empty ")
    private String userName;
    private int active;
    List<Role> roles;
}
