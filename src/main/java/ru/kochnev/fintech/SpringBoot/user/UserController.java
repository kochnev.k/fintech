package ru.kochnev.fintech.SpringBoot.user;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping("/registration")
    public void createNewUser(@RequestBody @Valid User user) {
        User userExists = userService.findUserById(user.getId());
        if (userExists == null) {
            userService.saveUser(user);
        }
    }
}
