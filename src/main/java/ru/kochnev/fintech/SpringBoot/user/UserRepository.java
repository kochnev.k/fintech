package ru.kochnev.fintech.SpringBoot.user;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
@Mapper
public interface UserRepository {

    @Select("SELECT user_id, active, user_name, password, FROM users WHERE user_id = #{id}")
    @Results(value = {
            @Result(column = "user_id", property = "id"),
            @Result(column = "active", property = "active"),
            @Result(column = "user_name", property = "userName"),
            @Result(column = "password", property = "password"),
            @Result(column = "user_id", property = "roles", javaType = List.class,
                    many = @Many(select = "ru.kochnev.fintech.SpringBoot.userrole.UserRoleRepository.getRolesByUserId"))
    })
    User findUserById(UUID id);

    @Select("SELECT user_id, active, user_name, password, FROM users WHERE user_name = #{userName}")
    @Results(value = {
            @Result(column = "user_id", property = "id"),
            @Result(column = "active", property = "active"),
            @Result(column = "user_name", property = "userName"),
            @Result(column = "password", property = "password"),
            @Result(column = "user_id", property = "roles", javaType = List.class,
                    many = @Many(select = "ru.kochnev.fintech.SpringBoot.userrole.UserRoleRepository.getRolesByUserId"))
    })
    User findUserByUsername(String userName);

    @Insert("INSERT INTO users (user_id, active, user_name, password) " +
            "VALUES(#{id}::UUID, #{active}, #{userName}, #{password})")
    int setUserInfo(User user);
}
