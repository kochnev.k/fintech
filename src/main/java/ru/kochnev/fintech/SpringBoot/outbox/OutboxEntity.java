package ru.kochnev.fintech.SpringBoot.outbox;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class OutboxEntity {

    private UUID id;

    private String payload;

    private Status status;

    public enum Status {
        SAVE, UPDATE, DELETE
    }
}
