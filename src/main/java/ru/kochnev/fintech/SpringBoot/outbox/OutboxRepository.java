package ru.kochnev.fintech.SpringBoot.outbox;

import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.UUID;

@Mapper
public interface OutboxRepository {

    @Insert("INSERT INTO outbox (id, payload, status) " +
            "VALUES (#{id}::uuid, #{payload}, #{status}) LIMIT 50")
    int insert(OutboxEntity outboxEntity);

    @Select("SELECT * FROM outbox")
    @Results(value = {
            @Result(column = "id", property = "id"),
            @Result(property = "payload", column = "payload"),
            @Result(property = "status", column = "status")
    })
    List<OutboxEntity> findAll();

    @Update("<script> " +
                "DELETE FROM outbox " +
                    "<where> id IN (" +
                        "<foreach item = 'item' index = 'index' collection = 'list' " +
                            "separator = ','> " +
                            "#{item}::uuid " +
                        "</foreach>" +
                        ") " +
                    "</where> " +
            "</script>")
    int deleteById(@Param("list") List<UUID> idList);



}
