package ru.kochnev.fintech.SpringBoot.outbox;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class OutboxService {

    private final OutboxRepository outboxRepository;

    public void save(String payload, OutboxEntity.Status status) {
        outboxRepository.insert(new OutboxEntity(UUID.randomUUID(), payload, status));
    }

    public void deleteById(List<UUID> idList) {
        outboxRepository.deleteById(idList);
    }

    public List<OutboxEntity> findAll() {
        return outboxRepository.findAll();
    }




}
