package ru.kochnev.fintech.SpringBoot.typeHandlers;

import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import ru.kochnev.fintech.SpringBoot.outbox.OutboxEntity;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@MappedTypes({OutboxEntity.Status.class})
public class OutboxEntityStatusTypeHandler extends BaseTypeHandler<OutboxEntity.Status> {

    @Override
    public void setNonNullParameter(PreparedStatement preparedStatement, int i, OutboxEntity.Status status, JdbcType jdbcType) throws SQLException {
        preparedStatement.setString(i, status.toString());
    }

    @Override
    public OutboxEntity.Status getNullableResult(ResultSet resultSet, String s) throws SQLException {
        return OutboxEntity.Status.valueOf(resultSet.getObject(s).toString());
    }

    @Override
    public OutboxEntity.Status getNullableResult(ResultSet resultSet, int i) throws SQLException {
        return resultSet.getObject(i, OutboxEntity.Status.class);
    }

    @Override
    public OutboxEntity.Status getNullableResult(CallableStatement callableStatement, int i) throws SQLException {
        return callableStatement.getObject(i, OutboxEntity.Status.class);
    }
}
