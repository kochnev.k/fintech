package ru.kochnev.fintech.SpringBoot.courseLRUCache;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Component
@Getter
public class CourseLRUCache<K, V> {

    @Value("${cache.course.size}")
    private int cacheSize;
    private final Map<K, V> cacheMap = new LinkedHashMap<>(cacheSize, 0.75f, true) {
        @Override
        protected boolean removeEldestEntry(Map.Entry eldest) {
            return size() > cacheSize;
        }
    };
    private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();


    public V put(K key, V value) {
        this.lock.writeLock().lock();
        try {
            return cacheMap.put(key, value);
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    public V get(K key) {
        this.lock.readLock().lock();
        try {
            return cacheMap.get(key);
        } finally {
        this.lock.readLock().unlock();
        }
    }

    public V remove(K key) {
        this.lock.writeLock().lock();
        try {
            return cacheMap.remove(key);
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    public V replace(K key, V value) {
        this.lock.writeLock().lock();
        try {
            return cacheMap.replace(key, value);
        } finally {
            this.lock.writeLock().unlock();
        }
    }
}


