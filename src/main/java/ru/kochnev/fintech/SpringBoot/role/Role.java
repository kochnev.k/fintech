package ru.kochnev.fintech.SpringBoot.role;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.UUID;

@Data
@NoArgsConstructor
public class Role implements GrantedAuthority {
    private UUID id;
    private String name;

    @Override
    public String getAuthority() {
        return name;
    }
}