package ru.kochnev.fintech.SpringBoot;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.lifecycle.Startables;
import ru.kochnev.fintech.SpringBoot.course.Course;
import ru.kochnev.fintech.SpringBoot.course.CourseRequest;
import ru.kochnev.fintech.SpringBoot.student.StudentRequest;

import java.util.UUID;
import java.util.stream.Stream;


@SpringBootTest
public class ApplicationTest {

    public static PostgreSQLContainer<?> postgresContainer = new PostgreSQLContainer<>("postgres:11.1");

    static {
        Startables.deepStart(Stream.of(postgresContainer)).join();
        System.setProperty("spring.datasource.url", postgresContainer.getJdbcUrl());
        System.setProperty("spring.datasource.username", postgresContainer.getUsername());
        System.setProperty("spring.datasource.password", postgresContainer.getPassword());
    }

    @Autowired
    private Flyway flyway;

    public void cleanAndMigrate() {
        flyway.clean();
        flyway.migrate();
    }

    public StudentRequest createTestStudentRequest() {

        String studentId = "a920b050-a081-4463-9851-e52e9cd9c68c";

        CourseRequest courseRequestTest = createTestCourseRequest();

        StudentRequest studentRequestTest = new StudentRequest()
                .setId(UUID.fromString(studentId))
                .setName("Testov Test Testovich")
                .setAge(19)
                .setGrade(1)
                .setTimeFrom(9)
                .setTimeTo(13)
                .setCourseRequest(courseRequestTest);

        return studentRequestTest;
    }

    public CourseRequest createTestCourseRequest() {

        String courseId = "065cbe0a-3fc2-45ed-9c10-1fb2852242f2";

        CourseRequest courseRequestTest = new CourseRequest()
                .setId(UUID.fromString(courseId))
                .setName("Test Course")
                .setDescription("About test course")
                .setRequiredGrade(0);

        return courseRequestTest;
    }
}


