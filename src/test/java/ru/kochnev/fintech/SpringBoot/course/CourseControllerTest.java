package ru.kochnev.fintech.SpringBoot.course;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.kochnev.fintech.SpringBoot.ApplicationTest;
import ru.kochnev.fintech.SpringBoot.exceptions.MyException;
import ru.kochnev.fintech.SpringBoot.student.Student;
import ru.kochnev.fintech.SpringBoot.student.StudentRepository;
import ru.kochnev.fintech.SpringBoot.student.StudentRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
class CourseControllerTest extends ApplicationTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CourseRepository courseRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private MockMvc mockMvc;

    @AfterEach
    void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    void save() throws Exception{

        CourseRequest courseRequestTest = createTestCourseRequest();

        String expectedContent = objectMapper.writeValueAsString(courseRequestTest);

        this.mockMvc.perform(post("/course")
                        .content(expectedContent)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void getById() throws Exception{

        CourseRequest courseRequestTest = createTestCourseRequest();

        String expectedContent = objectMapper.writeValueAsString(courseRequestTest);

        this.mockMvc.perform(get("/course/" + courseRequestTest.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void update() throws Exception {

        CourseRequest courseRequestUpdated = createTestCourseRequest();
        courseRequestUpdated
                .setName("Java test course")
                .setDescription("About Java test course");

        String requestContent = objectMapper.writeValueAsString(courseRequestUpdated);

        this.mockMvc.perform(put("/course")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isOk());

        Course expectedCourse = new Course();
        expectedCourse
                .setId(courseRequestUpdated.getId())
                .setName("Java test course")
                .setDescription("About Java test course")
                .setRequiredGrade(0);

        Course actualCourse = courseRepository.findById(courseRequestUpdated.getId());

        Assertions.assertEquals(expectedCourse, actualCourse);
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void deleteById() throws Exception {

        CourseRequest courseRequestTest = createTestCourseRequest();

        mockMvc.perform(delete("/course/" + courseRequestTest.getId().toString()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    void addStudentToCourse() throws Exception {

        Course newCourse = new Course();
        newCourse
                .setName("Java test course")
                .setDescription("About Java test course")
                .setRequiredGrade(0);

        courseRepository.insert(newCourse);

        StudentRequest studentRequestTest = createTestStudentRequest();

        List<String> studentIdList = new ArrayList<>();
        studentIdList.add(studentRequestTest.getId().toString());

        String requestContent = objectMapper.writeValueAsString(studentIdList);

        this.mockMvc.perform(patch("/course/" + newCourse.getId().toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isOk());

        Student expectedStudent = new Student();
        expectedStudent
                .setId(studentRequestTest.getId())
                .setName(studentRequestTest.getName())
                .setAge(studentRequestTest.getAge())
                .setGrade(studentRequestTest.getGrade())
                .setTimeFrom(studentRequestTest.getTimeFrom())
                .setTimeTo(studentRequestTest.getTimeTo())
                .setCourse(newCourse);

        Student actualStudent = studentRepository.findById(studentRequestTest.getId());

        Assertions.assertEquals(expectedStudent, actualStudent);
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void getByNotExistingId() throws Exception {

        UUID notExistingCourseId = UUID.randomUUID();

        this.mockMvc.perform(get("/course/" + notExistingCourseId))
                .andExpect(status().isNotFound())
                .andExpect(mvcResult -> Objects.requireNonNull(mvcResult.getResolvedException()).getClass().equals(MyException.class));
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    public void saveWithEmptyField() throws Exception {

        CourseRequest courseRequestTest = createTestCourseRequest();
        courseRequestTest.setName("");

        String requestContent = objectMapper.writeValueAsString(courseRequestTest);

        mockMvc.perform(MockMvcRequestBuilders.post("/course")
                        .content(requestContent)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("name of course cannot be empty "));
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    public void gradeLessRequiredGrade() throws Exception {

        Course newCourse = new Course();
        newCourse
                .setName("Java test course")
                .setDescription("About Java test course")
                .setRequiredGrade(7);

        courseRepository.insert(newCourse);

        StudentRequest studentRequestTest = createTestStudentRequest();

        List<String> studentIdList = new ArrayList<>();
        studentIdList.add(studentRequestTest.getId().toString());

        String requestContent = objectMapper.writeValueAsString(studentIdList);

        mockMvc.perform(patch("/course/" + newCourse.getId().toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestContent))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("Higher grade required"));
    }

    @Test
    @WithMockUser(username="user", roles={"USER"})
    void roleWrong() throws Exception{

        CourseRequest courseRequestTest = createTestCourseRequest();

        String expectedContent = objectMapper.writeValueAsString(courseRequestTest);

        this.mockMvc.perform(post("/course")
                        .content(expectedContent)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="user", roles={"USER"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void roleRight() throws Exception{

        CourseRequest courseRequestTest = createTestCourseRequest();

        String expectedContent = objectMapper.writeValueAsString(courseRequestTest);

        this.mockMvc.perform(get("/course/" + courseRequestTest.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    void unauthorizedUser() throws Exception{

        CourseRequest courseRequestTest = createTestCourseRequest();

        String expectedContent = objectMapper.writeValueAsString(courseRequestTest);

        this.mockMvc.perform(post("/course")
                        .with(httpBasic("admin", "wrongPassword"))
                        .content(expectedContent)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    void authorizedUser() throws Exception{

        CourseRequest courseRequestTest = createTestCourseRequest();

        String expectedContent = objectMapper.writeValueAsString(courseRequestTest);

        this.mockMvc.perform(post("/course")
                        .with(httpBasic("admin", "admin"))
                        .content(expectedContent)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}

