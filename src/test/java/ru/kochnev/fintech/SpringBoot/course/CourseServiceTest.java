package ru.kochnev.fintech.SpringBoot.course;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.kochnev.fintech.SpringBoot.ApplicationTest;
import ru.kochnev.fintech.SpringBoot.courseLRUCache.CourseLRUCache;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;

class CourseServiceTest extends ApplicationTest {

    @Autowired
    private CourseService courseService;
    @Autowired
    private ModelMapper modelMapper;
    @Autowired
    private CourseLRUCache<UUID, Course> courseLRUCache;


    @AfterEach
    void tearDown() {
        cleanAndMigrate();
        courseLRUCache.getCacheMap().clear();
    }


    @Test
    void save() {

        CourseRequest courseRequestTest = createTestCourseRequest();
        Course expectedCourse = modelMapper.map(courseRequestTest, Course.class);
        if (!courseLRUCache.getCacheMap().containsKey(expectedCourse.getId())) {
            courseService.save(courseRequestTest);
        }

        Course actualCourse = courseLRUCache.get(courseRequestTest.getId());

        assertEquals(expectedCourse, actualCourse);
    }

    @Test
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void update() {

        CourseRequest courseRequestTest = createTestCourseRequest();
        Course courseTest = modelMapper.map(courseRequestTest, Course.class);
        courseLRUCache.put(courseTest.getId(), courseTest);

        CourseRequest courseRequestUpdated = createTestCourseRequest();
        courseRequestUpdated
                .setName("Java test course")
                .setDescription("About Java test course");
        Course expectedCourse = modelMapper.map(courseRequestUpdated, Course.class);

        courseService.update(courseRequestUpdated);

        Course actualCourse = courseLRUCache.get(courseRequestUpdated.getId());

        assertEquals(expectedCourse, actualCourse);
    }

    @Test
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void deleteById() {

        CourseRequest courseRequestTest = createTestCourseRequest();
        Course expectedCourse = modelMapper.map(courseRequestTest, Course.class);
        courseLRUCache.put(expectedCourse.getId(), expectedCourse);

        courseService.deleteById(expectedCourse.getId().toString());

        Course actualCourse = courseLRUCache.get(expectedCourse.getId());

        assertNull(actualCourse);
    }


    @Test
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void getCourseExistNotInCache() {

        CourseService spyCourseService = spy(courseService);
        CourseRequest courseRequestTest = createTestCourseRequest();
        Course expectedCourse = modelMapper.map(courseRequestTest, Course.class);

        if (!courseLRUCache.getCacheMap().containsKey(expectedCourse.getId())) {
            spyCourseService.getById(expectedCourse.getId().toString());
        }
        Course actualCourse = courseLRUCache.get(expectedCourse.getId());

        verify(spyCourseService, times(1)).findById(expectedCourse.getId());
        assertEquals(expectedCourse, actualCourse);
    }

    @Test
    void getCourseExistInCache() {

        CourseService spyCourseService = spy(courseService);
        CourseRequest courseRequestTest = createTestCourseRequest();
        Course expectedCourse = modelMapper.map(courseRequestTest, Course.class);
        courseLRUCache.put(expectedCourse.getId(), expectedCourse);

        spyCourseService.getById(expectedCourse.getId().toString());

        Course actualCourse = courseLRUCache.get(expectedCourse.getId());

        verify(spyCourseService, times(0)).findById(expectedCourse.getId());
        assertEquals(expectedCourse, actualCourse);
    }
}