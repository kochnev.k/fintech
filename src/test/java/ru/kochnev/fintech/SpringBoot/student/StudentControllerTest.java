package ru.kochnev.fintech.SpringBoot.student;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.kochnev.fintech.SpringBoot.ApplicationTest;
import ru.kochnev.fintech.SpringBoot.course.Course;
import ru.kochnev.fintech.SpringBoot.exceptions.MyException;

import java.util.Objects;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
class StudentControllerTest extends ApplicationTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private JdbcTemplate jdbcTemplate;



    @AfterEach
    void tearDown() {
        cleanAndMigrate();
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void save() throws Exception {

        StudentRequest studentRequestTest = createTestStudentRequest();

        String expectedContent = objectMapper.writeValueAsString(studentRequestTest);

        this.mockMvc.perform(
                        post("/student")
                                .content(expectedContent)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    void getById() throws Exception {

        StudentRequest studentRequestTest = createTestStudentRequest();

        String expectedContent = objectMapper.writeValueAsString(studentRequestTest);

        this.mockMvc.perform(get("/student/" + studentRequestTest.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    void update() throws Exception {

        StudentRequest studentRequestUpdated = createTestStudentRequest();
        studentRequestUpdated
                .setGrade(4)
                .setTimeFrom(7)
                .setTimeTo(7);

        String requestContent = objectMapper.writeValueAsString(studentRequestUpdated);

        this.mockMvc.perform(put("/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestContent))
                .andExpect(status().isOk());

        Course course = new Course();
        course
                .setId(studentRequestUpdated.getCourseRequest().getId())
                .setName("Test Course")
                .setDescription("About test course")
                .setRequiredGrade(0);

        Student expectedStudent = new Student();
        expectedStudent
                .setId(studentRequestUpdated.getId())
                .setName("Testov Test Testovich")
                .setAge(19)
                .setGrade(4)
                .setTimeFrom(7)
                .setTimeTo(7)
                .setCourse(course);

        Student actualStudent = studentRepository.findById(studentRequestUpdated.getId());

        Assertions.assertEquals(expectedStudent, actualStudent);
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    void deleteById() throws Exception {

        StudentRequest studentRequestTest = createTestStudentRequest();

        mockMvc.perform(delete("/student/" + studentRequestTest.getId().toString()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    void getByNotExistingId() throws Exception {

        UUID notExistingStudentId = UUID.randomUUID();

        this.mockMvc.perform(get("/student/" + notExistingStudentId))
                .andExpect(status().isNotFound())
                .andExpect(mvcResult -> Objects.requireNonNull(mvcResult.getResolvedException()).getClass().equals(MyException.class));
    }

    @Test
    @WithMockUser(username="admin",roles={"ADMIN"})
    public void saveWithEmptyField() throws Exception {

        StudentRequest studentRequestTest = createTestStudentRequest();
        studentRequestTest.setName("");

        String requestContent = objectMapper.writeValueAsString(studentRequestTest);

        mockMvc.perform(MockMvcRequestBuilders.post("/student")
                        .content(requestContent)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$").value("name of student cannot be empty "));
    }

    @Test
    @WithMockUser(username="user", roles={"USER"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0)")
    void roleWrong() throws Exception {

        StudentRequest studentRequestTest = createTestStudentRequest();

        String expectedContent = objectMapper.writeValueAsString(studentRequestTest);

        this.mockMvc.perform(
                        post("/student")
                                .content(expectedContent)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser(username="user", roles={"USER"})
    @Sql(statements = "INSERT INTO courses(id, name, description, required_grade) VALUES(uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'), 'Test Course', 'About test course', 0); " +
            "INSERT INTO students(id, name, age, grade, time_from, time_to, course_id) VALUES(uuid('a920b050-a081-4463-9851-e52e9cd9c68c'), 'Testov Test Testovich', 19, 1, 9, 13, uuid('065cbe0a-3fc2-45ed-9c10-1fb2852242f2'))")
    void roleRight() throws Exception {

        StudentRequest studentRequestTest = createTestStudentRequest();

        String expectedContent = objectMapper.writeValueAsString(studentRequestTest);

        this.mockMvc.perform(get("/student/" + studentRequestTest.getId().toString()))
                .andExpect(status().isOk())
                .andExpect(content().string(expectedContent));
    }
}

