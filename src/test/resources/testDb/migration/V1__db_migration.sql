CREATE TABLE IF NOT EXISTS roles (
    role_id           UUID            NOT NULL,
    role              VARCHAR(255)    DEFAULT NULL,
    PRIMARY KEY (role_id)
);
INSERT INTO roles VALUES ('0f18acb3-5d62-4219-98fd-a229c25f4499', 'ROLE_ADMIN');
INSERT INTO roles VALUES ('4296f816-53b6-4b19-94ff-f2a6768d38a7', 'ROLE_USER');

CREATE TABLE IF NOT EXISTS users (
    user_id           UUID            NOT NULL,
    active            INT             DEFAULT 0,
    user_name         VARCHAR(255)    NOT NULL,
    password          VARCHAR(255)    NOT NULL,
    PRIMARY KEY (user_id)
);
INSERT INTO users VALUES ('3dccddc9-a5dd-459d-b476-cd7e42cba8eb', 1, 'admin', 'admin', '$2a$10$tSeHGqlg8d5QY7MwSusx/uyGWfYZ8daUA8cClaBibI47x9s4VpfD.');

CREATE TABLE IF NOT EXISTS user_role (
    user_id           UUID            NOT NULL,
    role_id           UUID            NOT NULL,
    PRIMARY KEY (user_id,role_id)
);
INSERT INTO user_role VALUES (
    (SELECT user_id FROM users WHERE login_id = 'admin'),
    (SELECT role_id FROM roles WHERE role = 'ROLE_ADMIN')
);


CREATE TABLE IF NOT EXISTS courses (
    id                UUID            NOT NULL PRIMARY KEY,
    name              VARCHAR(100)    NOT NULL,
    description       VARCHAR(255)    NOT NULL,
    required_grade    INT             NOT NULL
);

CREATE TABLE IF NOT EXISTS students (
    id                UUID            NOT NULL PRIMARY KEY,
    name              VARCHAR(100)    NOT NULL,
    age               INT             NOT NULL,
    grade             INT             NULL,
    time_from         INT             NOT NULL,
    time_to           INT             NOT NULL,
    course_id         UUID            NULL,
    FOREIGN KEY(course_id) REFERENCES courses(id)
);
CREATE INDEX course_id_idx ON students(course_id);